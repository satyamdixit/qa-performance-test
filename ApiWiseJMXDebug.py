#import the Element Tree package
import xml.etree.ElementTree as ET

def modifyfile(filepath,base,port,relative,newFile,var,csvFile,csvBool):

    #Fetch the xml file and parse it to ET 
    tree = ET.parse(filepath)

    #Get the root of the xml
    root=tree.getroot()

    #Finding the exact location of the desired stringpop
    baseURL=root[0][1][1][0][1]
    basePort=root[0][1][1][0][2]
    relativePath=root[0][1][1][0][7]
    csvName=root[0][1][1][4][2]
    varName=root[0][1][1][4][7]
    en=root[0][1][1][4]
    print("BaseURL before modification was:")
    print(baseURL.text)
    print("BasePort before modification was:")
    print(basePort.text)
    print("RelativePath before modification was:")
    print(relativePath.text)

    #modify the base url,port,relative path,csvFile name and variable name
    baseURL.text=str(base)

    basePort.text=str(port)

    relativePath.text=str(relative)

    csvName.text=str(csvFile)

    varName.text=str(var)


    en.set("enabled",csvBool)

    #Save the changes and make a new file
    tree.write(newFile)

def readDataFromFile(filepath):
    with open(filepath,"r") as f:
        contents=f.read()
        for row in contents.split("\n"):
            n=len(row.split("\t"))
            if n>5:
                file=row.split("\t")[0]
                base=row.split("\t")[1]
                port=row.split("\t")[2]
                relative=row.split("\t")[3]
                newFile=row.split("\t")[4]
                var=row.split("\t")[5]
                csvFile=row.split("\t")[6]
                modifyfile(file,base,port,relative,newFile,var,csvFile,"true")
            else:
                file=row.split("\t")[0]
                base=row.split("\t")[1]
                port=row.split("\t")[2]
                relative=row.split("\t")[3]
                newFile=row.split("\t")[4]
                modifyfile(file,base,port,relative,newFile,"","","false")

#def readDataForHeaders(fileforheaders):
#    with open(fileforheaders,"r") as h:
        

readDataFromFile(input("Enter the data file path:"))
